use std::collections::HashSet;

use bytes::Bytes;
use chrono::prelude::*;
use feed_rs::{
    model::{Feed as LibFeed, Text},
    parser,
};
use log::info;
use reqwest;
use url::Url;

use crate::errors::ToLogError;

mod feed;
mod feed_entry;

pub use feed::Feed;
pub use feed_entry::{FeedEntry, FeedEntryUrl};

pub struct FeedParser {
    feed: LibFeed,
    url: String,
}

struct FeedTitle;

impl FeedParser {
    pub async fn new(url: String) -> Result<Self, ToLogError> {
        let content = Self::get_feed_content(&url).await?;
        let feed = parser::parse(&content[..])?;
        Ok(Self { feed, url })
    }

    async fn get_feed_content(url: &str) -> Result<Bytes, reqwest::Error> {
        Ok(reqwest::get(url).await?.bytes().await?)
    }

    pub fn parse_feed(self, max_age: &DateTime<Utc>, entries: HashSet<String>) -> Feed {
        let mut feed = Feed::new(&self.feed.title, self.url);
        feed.load_entries(self.feed, max_age, entries);
        feed
    }
}

impl FeedTitle {
    pub fn get_feed_title(title: &Option<Text>, url: &str) -> String {
        match Self::get_title(title) {
            Some(title) => title,
            None => Self::get_title_from_url(url),
        }
    }

    pub fn get_entry_title(title: Option<Text>, time: &DateTime<Utc>) -> String {
        Self::get_title(&title).unwrap_or(format!("Entry posted {}", time.format("%v %T")))
    }

    fn get_title(title: &Option<Text>) -> Option<String> {
        match title {
            Some(title) if title.content.len() > 0 => Some(title.content.clone()),
            Some(_) | None => None,
        }
    }

    fn get_title_from_url(url: &str) -> String {
        let parsed_url = Url::parse(url);
        match parsed_url {
            Err(e) => {
                info!("could not parse feed url: {}", e);
                url.to_string()
            }
            Ok(u) => match u.host_str() {
                None => {
                    info!("could not find feed host: {}", url);
                    url.to_string()
                }
                Some(host) => host.to_string(),
            },
        }
    }
}
