use std::collections::{HashMap, HashSet};

use chrono::prelude::*;
use feed_rs::model::{Entry, Feed as LibFeed, Text};

use super::{FeedEntry, FeedTitle};

pub struct Feed {
    pub title: String,
    pub new_entries: Vec<FeedEntry>,
    pub new_dates: HashMap<String, DateTime<Utc>>,
}

impl Feed {
    pub fn new(title: &Option<Text>, url: String) -> Self {
        Self {
            title: FeedTitle::get_feed_title(title, &url),
            new_entries: Vec::new(),
            new_dates: HashMap::new(),
        }
    }

    pub fn load_entries(
        &mut self,
        feed: LibFeed,
        max_age: &DateTime<Utc>,
        mut entries: HashSet<String>,
    ) {
        let now = Utc::now();
        for entry in feed.entries.into_iter() {
            let entry_time = self.get_time(&entry, &now);
            if entry_time > *max_age {
                match entries.get(&entry.id) {
                    None => {
                        let entry = FeedEntry::new(entry, entry_time);
                        entries.insert(entry.id.clone());
                        self.new_entries.push(entry);
                    }
                    Some(_) => {
                        self.new_dates.insert(entry.id, entry_time);
                    }
                }
            }
        }
    }

    fn get_time(&self, entry: &Entry, now: &DateTime<Utc>) -> DateTime<Utc> {
        if let Some(date) = entry.published {
            date
        } else if let Some(date) = entry.updated {
            date
        } else {
            now.clone()
        }
    }
}
