use chrono::prelude::*;
use feed_rs::model::Entry;

use super::FeedTitle;

mod feed_entry_url;
pub use feed_entry_url::FeedEntryUrl;

pub struct FeedEntry {
    pub id: String,
    pub title: String,
    pub urls: Vec<FeedEntryUrl>,
    pub body: Option<String>,
    pub time: DateTime<Utc>,
}

impl FeedEntry {
    pub fn new(entry: Entry, time: DateTime<Utc>) -> Self {
        Self {
            urls: Self::get_urls(&entry),
            body: Self::get_body(&entry),
            id: entry.id,
            title: FeedTitle::get_entry_title(entry.title, &time),
            time,
        }
    }

    fn get_urls(entry: &Entry) -> Vec<FeedEntryUrl> {
        entry.links.iter().map(|l| FeedEntryUrl::new(l)).collect()
    }

    fn get_body(entry: &Entry) -> Option<String> {
        entry
            .content
            .as_ref()
            .and_then(|content| content.body.as_ref())
            .map(|c| c.to_string())
            .or_else(|| entry.summary.as_ref().map(|s| s.content.to_string()))
    }
}
