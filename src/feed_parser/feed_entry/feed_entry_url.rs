use feed_rs::model::Link;

pub struct FeedEntryUrl {
    pub url: String,
    pub title: String,
}

impl FeedEntryUrl {
    pub fn new(url: &Link) -> Self {
        Self {
            url: url.href.clone(),
            title: Self::get_url_title(url),
        }
    }

    fn get_url_title(url: &Link) -> String {
        url.title
            .as_ref()
            .map(|t| t.to_string())
            .unwrap_or(url.href.to_string())
    }
}
