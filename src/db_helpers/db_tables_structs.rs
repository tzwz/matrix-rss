use std::{collections::HashSet, string::ToString};

use chrono::prelude::*;
use rusqlite::{params, Connection, Error as RusqliteError, Result as RusqliteResult, Row};

use super::{ToLogError, ToMatrixError};

pub struct FeedInfo {
    pub id: i32,
    pub url: String,
    pub is_scanned: bool,
}

pub struct FeedRoom {
    feed_id: i32,
    pub matrix_room: String,
    pub title: Option<String>,
    pub show_content: bool,
}

pub struct RoomFeed {
    pub feed_id: i32,
    pub url: String,
    pub title: Option<String>,
}

pub struct FeedEntry {
    pub feed_id: i32,
    pub entry_id: String,
    pub time: DateTime<Utc>,
}

impl FeedInfo {
    pub fn new(row: &Row<'_>) -> RusqliteResult<Self> {
        Ok(Self {
            id: row.get(0)?,
            url: row.get(1)?,
            is_scanned: row.get(2)?,
        })
    }

    pub fn get_id_by_url(conn: &Connection, url: &str) -> Result<i32, ToMatrixError> {
        let feed_id = Self::get_id_from_url(conn, url);
        match feed_id {
            Ok(id) => Ok(id),
            Err(RusqliteError::QueryReturnedNoRows) => {
                conn.execute(
                    "INSERT INTO FeedInfo (url, is_scanned) VALUES (?1, ?2)",
                    params![url, false],
                )?;
                Ok(Self::get_id_from_url(conn, url)?)
            }
            Err(e) => Err(e.into()),
        }
    }

    fn get_id_from_url(conn: &Connection, url: &str) -> RusqliteResult<i32> {
        conn.query_row(
            "SELECT id FROM FeedInfo WHERE url = ?1",
            params![url],
            |row| Ok(row.get(0)?),
        )
    }

    pub fn update_scanned(&self, conn: &Connection) -> Result<(), ToLogError> {
        if !self.is_scanned {
            conn.execute(
                "UPDATE FeedInfo SET is_scanned = TRUE WHERE id = ?1",
                params![self.id],
            )?;
        }
        Ok(())
    }

    pub fn delete(conn: &Connection, feed_id: i32) -> Result<(), ToMatrixError> {
        conn.execute("DELETE FROM FeedInfo WHERE id = ?1", params![feed_id])?;
        Ok(())
    }
}

impl FeedRoom {
    pub fn new<T>(feed_id: i32, matrix_room: T, title: Option<String>, show_content: bool) -> Self
    where
        T: ToString,
    {
        Self {
            feed_id,
            matrix_room: matrix_room.to_string(),
            title,
            show_content,
        }
    }

    pub fn get_feed_rooms(conn: &Connection, feed_id: i32) -> RusqliteResult<Vec<FeedRoom>> {
        let mut stmt = conn.prepare("SELECT * FROM FeedRoom WHERE feed_id = ?1")?;
        let rooms_results = stmt.query_map(params![feed_id], |row| {
            Ok(Self::new(
                feed_id,
                row.get::<usize, String>(1)?,
                row.get(2)?,
                row.get(3)?,
            ))
        })?;
        let mut rooms = Vec::new();
        for room in rooms_results {
            rooms.push(room?);
        }
        Ok(rooms)
    }

    pub fn add_to_db(&self, conn: &Connection) -> Result<(), ToMatrixError> {
        conn.execute(
            "INSERT INTO FeedRoom VALUES (
                ?1, ?2, ?3, ?4
            )",
            params![
                self.feed_id,
                &self.matrix_room,
                &self.title,
                self.show_content
            ],
        )?;
        Ok(())
    }

    pub fn change_title(
        self,
        conn: &Connection,
        new_title: Option<String>,
    ) -> Result<(), ToMatrixError> {
        conn.execute(
            "UPDATE FeedRoom SET title = ?1
             WHERE feed_id = ?2 AND matrix_room = ?3",
            params![new_title, self.feed_id, &self.matrix_room],
        )?;
        Ok(())
    }

    pub fn change_show_content(
        self,
        conn: &Connection,
        new_content: bool,
    ) -> Result<(), ToMatrixError> {
        conn.execute(
            "UPDATE FeedRoom SET show_content = ?1
             WHERE feed_id = ?2 AND matrix_room = ?3",
            params![new_content, self.feed_id, &self.matrix_room],
        )?;
        Ok(())
    }

    pub fn change_title_and_show_content(
        self,
        conn: &Connection,
        new_title: Option<String>,
        new_content: bool,
    ) -> Result<(), ToMatrixError> {
        conn.execute(
            "UPDATE FeedRoom SET title = ?1, show_content = ?2
             WHERE feed_id = ?3 AND matrix_room = ?4",
            params![new_title, new_content, self.feed_id, &self.matrix_room],
        )?;
        Ok(())
    }

    pub fn delete(self, conn: &Connection) -> Result<(), ToMatrixError> {
        conn.execute(
            "DELETE FROM FeedRoom WHERE feed_id = ?1 AND matrix_room = ?2",
            params![self.feed_id, &self.matrix_room],
        )?;
        Ok(())
    }

    pub fn delete_all(self, conn: &Connection) -> RusqliteResult<Vec<i32>> {
        let mut stmt = conn.prepare("SELECT feed_id FROM FeedRoom WHERE matrix_room = ?1")?;
        let mut ids_rows = stmt.query(params![self.matrix_room])?;
        let mut feeds_ids = Vec::new();
        while let Some(row) = ids_rows.next()? {
            feeds_ids.push(row.get(0)?);
        }
        conn.execute(
            "DELETE FROM FeedRoom WHERE matrix_room = ?1",
            params![&self.matrix_room],
        )?;
        Ok(feeds_ids)
    }
}

impl RoomFeed {
    pub fn get_room_feeds(
        conn: &Connection,
        room: &str,
    ) -> Result<Vec<RusqliteResult<RoomFeed>>, ToMatrixError> {
        let mut stmt = conn.prepare(
            "SELECT id, url, title
             FROM FeedRoom JOIN FeedInfo ON feed_id = id
             WHERE matrix_room = ?1",
        )?;
        let room = stmt
            .query_map(params![room], |row| Self::query_mapper(row))?
            .collect();
        Ok(room)
    }

    pub fn get_room_feed(
        conn: &Connection,
        room: &str,
        feed_url: &str,
    ) -> RusqliteResult<RoomFeed> {
        conn.query_row(
            "SELECT id, url, title
             FROM FeedRoom JOIN FeedInfo ON feed_id = id
             WHERE matrix_room = ?1 AND url = ?2",
            params![room, feed_url],
            |row| Self::query_mapper(row),
        )
    }

    fn query_mapper(row: &Row<'_>) -> RusqliteResult<Self> {
        Ok(Self {
            feed_id: row.get(0)?,
            url: row.get(1)?,
            title: row.get(2)?,
        })
    }
}

impl FeedEntry {
    pub fn new(feed_id: i32, entry_id: String, time: DateTime<Utc>) -> Self {
        Self {
            feed_id,
            entry_id,
            time,
        }
    }

    pub fn get_feed_entries(
        conn: &Connection,
        feed_id: i32,
    ) -> Result<HashSet<String>, ToLogError> {
        let mut stmt = conn.prepare("SELECT entry_id FROM FeedEntry WHERE feed_id = ?1")?;
        let rows = stmt.query_map(params![feed_id], |row| Ok(row.get(0)?))?;
        let mut rows_set = HashSet::new();
        for row in rows {
            let row = row?;
            rows_set.insert(row);
        }
        Ok(rows_set)
    }

    pub fn add_to_db(&self, conn: &Connection) -> Result<(), ToLogError> {
        conn.execute(
            "INSERT INTO FeedEntry VALUES (
                ?1, ?2, ?3
            )",
            params![self.feed_id, &self.entry_id, self.time],
        )?;
        Ok(())
    }

    pub fn update_time(
        &self,
        conn: &Connection,
        new_time: DateTime<Utc>,
    ) -> Result<(), ToLogError> {
        if new_time != self.time {
            conn.execute(
                "UPDATE FeedEntry SET time = ?1
                 WHERE feed_id = ?2 AND entry_id = ?3",
                params![self.time, self.feed_id, &self.entry_id],
            )?;
        }
        Ok(())
    }
}
