use std::env;

use rusqlite::{params, Connection, Error as RusqliteError, Result as RusqliteResult};

use matrix_sdk::ruma::RoomId;

use log::error;

mod db_tables_structs;
pub use self::db_tables_structs::*;

use super::{ToLogError, ToMatrixError};

pub enum ActionStatus {
    Updated,
    Added,
    Removed,
}

pub enum DbAction {
    Add,
    Remove,
    Missing,
}

pub fn get_db_connection() -> Result<Connection, ToMatrixError> {
    match Connection::open(env::var("MATRIX_RSS_DB_PATH").expect("db path")) {
        Ok(conn) => Ok(conn),
        Err(e) => Err(ToMatrixError::DbError(e)),
    }
}

pub fn add_to_db_or_update(
    url: &str,
    room_id: &RoomId,
    action: DbAction,
    show_content: Option<bool>,
    title: Option<String>,
) -> Result<ActionStatus, ToMatrixError> {
    let conn = get_db_connection()?;
    let room_id = room_id.as_str();
    let data = get_room_feed(&conn, room_id, url)?;
    match (action, data) {
        (DbAction::Add, Some(data)) => update_room_feed(&conn, room_id, data, show_content, title),
        (DbAction::Add, None) => add_room_feed(&conn, room_id, url, show_content, title),
        (DbAction::Remove, Some(data)) => remove_feed_from_room(&conn, room_id, data),
        (DbAction::Remove, None) => Err(ToMatrixError::NotInDbError),
        (DbAction::Missing, _) => {
            error!("something went terribly wrong with feed action: {}", url);
            Err(ToMatrixError::FeedActionMissingError)
        }
    }
}

fn get_room_feed(
    conn: &Connection,
    room_id: &str,
    url: &str,
) -> Result<Option<RoomFeed>, ToMatrixError> {
    let feed = RoomFeed::get_room_feed(conn, room_id, url);
    match feed {
        Ok(d) => Ok(Some(d)),
        Err(RusqliteError::QueryReturnedNoRows) => Ok(None),
        Err(e) => Err(e.into()),
    }
}

fn update_room_feed(
    conn: &Connection,
    room_id: &str,
    data: RoomFeed,
    show_content: Option<bool>,
    title: Option<String>,
) -> Result<ActionStatus, ToMatrixError> {
    let feed_room = FeedRoom::new(data.feed_id, room_id, None, false);
    match (title, show_content) {
        (Some(title), Some(content)) => {
            feed_room.change_title_and_show_content(conn, Some(title), content)?
        }
        (None, Some(content)) => feed_room.change_show_content(conn, content)?,
        (Some(title), None) => feed_room.change_title(conn, Some(title))?,
        (None, None) => feed_room.change_title(conn, None)?,
    };
    Ok(ActionStatus::Updated)
}

fn add_room_feed(
    conn: &Connection,
    room_id: &str,
    url: &str,
    show_content: Option<bool>,
    title: Option<String>,
) -> Result<ActionStatus, ToMatrixError> {
    let show_content = show_content.unwrap_or(false);
    let feed_id = FeedInfo::get_id_by_url(conn, url)?;
    FeedRoom::new(feed_id, room_id, title, show_content).add_to_db(conn)?;
    Ok(ActionStatus::Added)
}

fn remove_feed_from_room(
    conn: &Connection,
    room_id: &str,
    room_feed: RoomFeed,
) -> Result<ActionStatus, ToMatrixError> {
    FeedRoom::new(room_feed.feed_id, room_id, None, false).delete(conn)?;
    if !feed_has_rooms(conn, room_feed.feed_id)? {
        FeedInfo::delete(conn, room_feed.feed_id)?;
    }
    Ok(ActionStatus::Removed)
}

fn feed_has_rooms(conn: &Connection, feed_id: i32) -> Result<bool, ToMatrixError> {
    let mut stmt = conn.prepare("SELECT feed_id FROM FeedRoom WHERE feed_id = ?1")?;
    let rooms_amount = stmt
        .query_map(params![feed_id], |row| Ok(row.get(0)?))?
        .collect::<Vec<RusqliteResult<i32>>>()
        .len();
    Ok(rooms_amount > 0)
}

pub fn clear_room(conn: &Connection, room_id: &str) -> Result<(), ToLogError> {
    let feeds_ids = FeedRoom::new(0, room_id, None, false).delete_all(conn)?;
    for id in feeds_ids.into_iter() {
        match feed_has_rooms(conn, id) {
            Ok(has_rooms) => {
                if !has_rooms {
                    if let Err(e) = FeedInfo::delete(conn, id) {
                        error!("{:?}", e);
                    }
                }
            }
            Err(e) => error!("{:?}", e),
        }
    }
    Ok(())
}
