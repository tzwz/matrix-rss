use std::env;

use dotenvy::dotenv;
use env_logger::Builder;
use log::info;
use matrix_sdk::Client;
use rusqlite::Result;

mod background_loop;
mod db_helpers;
mod errors;
mod feed_parser;
mod handler;

use db_helpers::get_db_connection;
use errors::*;
use handler::Handler;

#[tokio::main]
async fn main() {
    dotenv().expect("Failed to load .env file");
    initialize_logger();
    create_db().expect("Failed to initialize db");
    let handler = create_handler().await;
    handler.run().await;
}

fn initialize_logger() {
    Builder::from_default_env().init();
    info!("initialized logging");
}

fn create_db() -> Result<()> {
    info!("creating db...");
    let conn = get_db_connection().expect("Db connection creation error");
    conn.execute(
        "CREATE TABLE IF NOT EXISTS FeedInfo (
            id              INTEGER PRIMARY KEY AUTOINCREMENT,
            url             TEXT UNIQUE NOT NULL,
            is_scanned      BOOLEAN NOT NULL
        )",
        [],
    )?;
    conn.execute(
        "CREATE TABLE IF NOT EXISTS FeedRoom (
            feed_id         INTEGER NOT NULL,
            matrix_room     TEXT NOT NULL,
            title           TEXT,
            show_content    BOOLEAN NOT NULL,
            PRIMARY KEY(feed_id, matrix_room),
            FOREIGN KEY(feed_id) REFERENCES FeedInfo(id) ON DELETE RESTRICT
        )",
        [],
    )?;
    conn.execute(
        "CREATE TABLE IF NOT EXISTS FeedEntry (
            feed_id         INTEGER,
            entry_id        TEXT,
            time            DATETIME NOT NULL,
            PRIMARY KEY(feed_id, entry_id),
            FOREIGN KEY(feed_id) REFERENCES FeedInfo(id) ON DELETE CASCADE
        )",
        [],
    )?;
    info!("db created!");
    Ok(())
}

async fn create_handler() -> Handler {
    info!("creating handler...");
    Handler::new(
        env::var("MATRIX_LOGIN").expect("missing login"),
        env::var("MATRIX_PASSWORD").expect("missing password"),
        Client::builder()
            .homeserver_url(
                env::var("MATRIX_HOMESERVER")
                    .expect("missing homeserver")
                    .as_str(),
            )
            .build()
            .await
            .expect("failed to create client"),
    )
}
