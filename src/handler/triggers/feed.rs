use std::str::SplitWhitespace;

use matrix_sdk::ruma::RoomId;

use url::Url;

use super::{
    db_helpers::{add_to_db_or_update, ActionStatus, DbAction},
    ToMatrixError, Trigger,
};

pub struct FeedTrigger;

impl Trigger for FeedTrigger {
    type Item = Self;

    fn new() -> Self {
        Self
    }

    fn activate(
        &self,
        message: SplitWhitespace<'_>,
        room_id: &RoomId,
    ) -> Result<String, ToMatrixError> {
        let (url, action, show_content, title) = self.get_values(message)?;
        let status = add_to_db_or_update(&url, room_id, action, show_content, title)?;
        match status {
            ActionStatus::Updated => Ok("Updated feed".into()),
            ActionStatus::Added => Ok("Added feed to db".into()),
            ActionStatus::Removed => Ok("Removed feed from db".into()),
        }
    }
}

impl FeedTrigger {
    fn get_values(
        &self,
        message: SplitWhitespace,
    ) -> Result<(String, DbAction, Option<bool>, Option<String>), ToMatrixError> {
        let mut url = String::new();
        let mut action = DbAction::Missing;
        let mut title = String::new();
        let mut show_content: Option<bool> = None;
        for val in message {
            let lowercased_val = val.to_lowercase();
            if !self.is_feed_url(&val, &mut url)
                && !self.is_feed_action(&lowercased_val, &mut action)
                && !self.is_show_content(&lowercased_val, &mut show_content)
            {
                title.push_str(&format!("{} ", val));
            }
        }
        if url.len() == 0 {
            Err(ToMatrixError::MissingUrl)
        } else if let DbAction::Missing = action {
            Err(ToMatrixError::MissingAction)
        } else {
            let title = if title.len() == 0 { None } else { Some(title) };
            Ok((url, action, show_content, title))
        }
    }

    fn is_feed_url(&self, val: &str, url: &mut String) -> bool {
        let parsed = Url::parse(val);
        match parsed {
            Err(_) => false,
            Ok(_) => {
                *url = val.to_string();
                true
            }
        }
    }

    fn is_feed_action(&self, val: &str, action: &mut DbAction) -> bool {
        let add_names = ["add", "insert", "update"];
        let remove_names = ["remove", "delete", "del", "rm"];
        if add_names.contains(&val) {
            *action = DbAction::Add;
            true
        } else if remove_names.contains(&val) {
            *action = DbAction::Remove;
            true
        } else {
            false
        }
    }

    fn is_show_content(&self, val: &str, show_content: &mut Option<bool>) -> bool {
        let show_names = ["show", "content", "+content"];
        let hide_names = ["hide", "nocontent", "no_content", "-content"];
        if show_names.contains(&val) {
            *show_content = Some(true);
            true
        } else if hide_names.contains(&val) {
            *show_content = Some(false);
            true
        } else {
            false
        }
    }
}
