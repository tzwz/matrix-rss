use std::str::SplitWhitespace;

use matrix_sdk::ruma::RoomId;

use super::{db_helpers, ToMatrixError};

mod feed;
mod list;
pub use self::{feed::FeedTrigger, list::ListTrigger};

pub trait Trigger {
    type Item;

    fn new() -> Self::Item;

    fn activate(
        &self,
        message: SplitWhitespace<'_>,
        room_id: &RoomId,
    ) -> Result<String, ToMatrixError>;
}
