use std::str::SplitWhitespace;

use rusqlite::Result as RusqliteResult;

use matrix_sdk::ruma::RoomId;

use log::error;

use super::{
    db_helpers::{get_db_connection, RoomFeed},
    ToMatrixError, Trigger,
};

pub struct ListTrigger;

impl Trigger for ListTrigger {
    type Item = Self;

    fn new() -> Self {
        Self
    }

    fn activate(&self, _: SplitWhitespace<'_>, room_id: &RoomId) -> Result<String, ToMatrixError> {
        let conn = get_db_connection()?;
        let room_feeds = RoomFeed::get_room_feeds(&conn, room_id.as_str())?;
        Ok(self.create_list(room_feeds))
    }
}

impl ListTrigger {
    fn create_list(&self, room_feeds: Vec<RusqliteResult<RoomFeed>>) -> String {
        let mut text = String::from("RSS feeds in this room:<br/>");
        let mut i = 1;
        for feed in room_feeds.iter() {
            match feed {
                Err(e) => error!("malformed data: {:?}", e),
                Ok(feed) => {
                    text.push_str(&format!(
                        "{}. {}{}<br/>",
                        i,
                        feed.url,
                        self.get_title(&feed)
                    ));
                    i += 1;
                }
            }
        }
        text
    }

    fn get_title(&self, feed: &RoomFeed) -> String {
        match &feed.title {
            Some(title) => format!(" -> <b>{}</b>", title),
            None => String::new(),
        }
    }
}
