use std::{str::SplitWhitespace, time::Duration};

use log::{error, info};
use matrix_sdk::{
    room::{Joined, Room},
    ruma::{
        events::{
            room::{
                member::StrippedRoomMemberEvent,
                message::{
                    MessageType, OriginalSyncRoomMessageEvent, RoomMessageEventContent,
                    TextMessageEventContent,
                },
            },
            AnySyncStateEvent,
        },
        RoomId,
    },
};

use crate::{
    db_helpers::{clear_room, get_db_connection},
    ToMatrixError,
};

use super::triggers::*;

#[non_exhaustive]
pub struct Responder {}

impl Responder {
    pub async fn process_response(event: OriginalSyncRoomMessageEvent, room: Room) {
        if let Room::Joined(room) = room {
            let MessageType::Text(text_content) = event.content.msgtype else {
                return;
            };
            if let Err(e) = Self::check_event(text_content, &room).await {
                error!("replying to msg error: {:?}", e);
            }
        }
    }

    pub async fn process_invite(_: StrippedRoomMemberEvent, room: Room) {
        if let Room::Invited(room) = room {
            tokio::spawn(async move {
                let mut delay = 1;
                while let Err(e) = room.accept_invitation().await {
                    error!("failed to join room: {}", e);
                    tokio::time::sleep(Duration::from_secs(delay)).await;
                    delay *= 2;
                    if delay > 3600 {
                        error!("Can't join room {} ({:?})", room.room_id(), e);
                        break;
                    }
                }
                info!("Successfully joined room {}", room.room_id());
            });
        }
    }

    pub async fn process_left(_: AnySyncStateEvent, room: Room) {
        if let Room::Left(room) = room {
            let room_id = room.room_id();
            Self::clean_room(&room_id);
            info!("cleaned up room with id: {}", room_id);
        }
    }

    async fn check_event(
        msg: TextMessageEventContent,
        room: &Joined,
    ) -> Result<(), matrix_sdk::Error> {
        if let Some(text) = Self::process_message(msg.body, room.room_id()) {
            room.send(RoomMessageEventContent::text_html("", text), None)
                .await?;
        }
        Ok(())
    }

    fn process_message(message: String, room_id: &RoomId) -> Option<String> {
        let mut message = message.split_whitespace();
        let trigger = message.next()?;
        let trigger_response = Self::get_trigger_response(trigger, message, room_id);
        Self::process_trigger_response(trigger_response)
    }

    fn get_trigger_response(
        trigger: &str,
        message: SplitWhitespace<'_>,
        room_id: &RoomId,
    ) -> Option<Result<String, ToMatrixError>> {
        match trigger.to_lowercase().as_str() {
            ".feed" => Some(FeedTrigger::new().activate(message, room_id)),
            ".list" | ".rsslist" | ".feedslist" | ".feedlist" | ".listfeed" | ".listfeeds" => {
                Some(ListTrigger::new().activate(message, room_id))
            }
            _ => None,
        }
    }

    fn process_trigger_response(
        trigger_response: Option<Result<String, ToMatrixError>>,
    ) -> Option<String> {
        match trigger_response? {
            Ok(text) => Some(text),
            Err(e) => Some(e.to_string()),
        }
    }

    fn clean_room(room_id: &RoomId) {
        let conn = match get_db_connection() {
            Ok(conn) => conn,
            Err(e) => {
                error!("{:?}", e);
                return;
            }
        };
        if let Err(e) = clear_room(&conn, room_id.as_str()) {
            error!("{:?}", e)
        }
    }
}
