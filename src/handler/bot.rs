use std::time::Duration;

use matrix_sdk::{
    config::SyncSettings,
    ruma::{events::room::message::RoomMessageEventContent, RoomId},
    Client,
};

use log::error;

use crate::errors::ToLogError;

use super::responder::Responder;

pub struct Bot {
    login: String,
    password: String,
    client: Client,
}

impl Bot {
    pub fn new<T, U>(login: T, password: U, client: Client) -> Self
    where
        T: ToString,
        U: ToString,
    {
        Self {
            login: login.to_string(),
            password: password.to_string(),
            client,
        }
    }

    pub async fn send_message(&self, text: impl Into<String>, room_id: &RoomId) {
        if let Some(room) = self.client.get_joined_room(room_id) {
            let send_result = room
                .send(RoomMessageEventContent::text_html("", text), None)
                .await;
            if let Err(e) = send_result {
                error!("{:?}", e);
            }
        }
    }

    pub async fn initialize(&self) -> Result<(), ToLogError> {
        self.send_login_request().await;
        self.login().await;
        self.client.sync_once(SyncSettings::default()).await?;
        Ok(())
    }

    pub async fn sync(&self) {
        self.client.add_event_handler(Responder::process_response);
        self.client.add_event_handler(Responder::process_invite);
        self.client.add_event_handler(Responder::process_left);
        let settings = SyncSettings::new().timeout(Duration::from_secs(30));
        if let Err(e) = self.client.sync(settings).await {
            error!("failed to sync: {}", e);
        }
    }

    async fn login(&self) {
        while !self.client.logged_in() {
            self.send_login_request().await;
            println!("failed to login, trying again in ten seconds");
            tokio::time::sleep(Duration::from_secs(10)).await;
        }
    }

    async fn send_login_request(&self) {
        let login_status = self
            .client
            .login_username(&self.login, &self.password)
            .send()
            .await;
        if let Err(e) = login_status {
            error!("{:?}", e);
        }
    }
}
