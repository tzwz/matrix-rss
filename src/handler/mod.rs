use std::sync::Arc;

use log::{error, info};
use matrix_sdk::Client;

use crate::{background_loop::background_loop, db_helpers, ToMatrixError};

mod bot;
pub mod responder;
pub mod triggers;

pub use bot::Bot;

pub struct Handler {
    bot: Arc<Bot>,
}

impl Handler {
    pub fn new<T, U>(login: T, password: U, client: Client) -> Self
    where
        T: ToString,
        U: ToString,
    {
        Self {
            bot: Arc::new(Bot::new(login, password, client)),
        }
    }

    pub async fn run(self) {
        info!("initial bot sync");
        match self.bot.initialize().await {
            Ok(()) => {
                info!("running handler...");
                let bot = self.bot.clone();
                info!("spawning rss loop in background...");
                tokio::spawn(async move {
                    background_loop(bot).await;
                });
                info!("starting to sync bot");
                self.bot.sync().await;
            }
            Err(e) => {
                error!("{}", e);
            }
        }
    }
}
