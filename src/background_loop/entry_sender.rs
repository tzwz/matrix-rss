use std::time::Duration;

use log::error;

use matrix_sdk::ruma::RoomId;

use super::{db_helpers::FeedRoom, Bot, FeedEntry, FeedEntryUrl, ToLogError};

pub struct EntrySender<'a, 'b> {
    bot: &'a Bot,
    feed_title: &'b str,
    rooms: Vec<FeedRoom>,
}

impl<'a, 'b> EntrySender<'a, 'b> {
    pub fn new(bot: &'a Bot, feed_title: &'b str, rooms: Vec<FeedRoom>) -> Self {
        Self {
            bot,
            feed_title,
            rooms,
        }
    }

    pub async fn send(&self, entry: FeedEntry) {
        let body = self.prepare_entry_body(&entry.body);
        let news = self.entry_to_message(entry);
        self.send_news(news, body).await;
    }

    fn prepare_entry_body(&self, body: &Option<String>) -> String {
        body.as_ref()
            .map(|b| format!("<br/>{}", b))
            .unwrap_or(String::new())
    }

    fn entry_to_message(&self, entry: FeedEntry) -> String {
        let mut text = String::new();
        let entry_title = self.add_title_info(entry.title);
        let entry_urls = self.add_urls(entry.urls);
        text.push_str(&entry_title);
        text.push_str(&entry_urls);
        text
    }

    fn add_title_info(&self, title: String) -> String {
        format!("Title: <b>{}</b><br/>", title)
    }

    fn add_urls(&self, urls: Vec<FeedEntryUrl>) -> String {
        if urls.len() == 1 {
            return format!("Url: {}<br/>", urls[0].url);
        }
        let mut urls_text = String::from("Urls:");
        for (i, url) in urls.into_iter().enumerate() {
            urls_text.push_str(&format!(
                "<b>[{}]</b> <a href=\"{}\">{}</a><br/>",
                i + 1,
                url.url,
                url.title
            ));
        }
        urls_text
    }

    async fn send_news(&self, news: String, body: String) {
        for room in self.rooms.iter() {
            let mut room_news = format!(
                "From: <b>{}</b><br/>{}",
                self.get_room_feed_title(room),
                news
            );
            if room.show_content {
                room_news.push_str(&body);
            }
            if let Err(e) = self.send_message(&room.matrix_room, room_news).await {
                error!("{:?}", e);
            }
        }
    }

    fn get_room_feed_title<'c>(&'c self, room: &'c FeedRoom) -> &'c str {
        if let Some(title) = &room.title {
            &title
        } else {
            self.feed_title
        }
    }

    async fn send_message(&self, matrix_room: &str, room_news: String) -> Result<(), ToLogError> {
        let room_id: Box<RoomId> = matrix_room.try_into()?;
        self.bot.send_message(room_news, &room_id).await;
        tokio::time::sleep(Duration::from_millis(500)).await;
        Ok(())
    }
}
