use std::{collections::HashMap, env, sync::Arc, time::Duration};

use log::{error, info};

use rusqlite::{Connection, Result as RusqliteResult};

use chrono::{DateTime, Duration as ChronoDuration, Utc};

use crate::{
    db_helpers::{self, get_db_connection, FeedInfo},
    errors::ToLogError,
    feed_parser::{FeedEntry, FeedEntryUrl, FeedParser},
    handler::Bot,
};

mod entry_sender;
use entry_sender::EntrySender;

pub async fn background_loop(bot: Arc<Bot>) {
    info!("rss loop starts");
    loop {
        if let Err(e) = check_feeds(&bot).await {
            error!("could not check feeds: {:?}", e);
        }
        tokio::time::sleep(Duration::from_secs(1800)).await;
    }
}

async fn check_feeds(bot: &Arc<Bot>) -> Result<(), ToLogError> {
    let conn = get_db_connection()?;
    let feeds_info = get_feeds_data(&conn)?;
    for maybe_feed in feeds_info.into_iter() {
        if let Err(e) = check_one_feed(bot, maybe_feed).await {
            error!("could not check feed: {:?}", e);
        }
    }
    delete_old_entries(&conn);
    Ok(())
}

fn get_feeds_data(conn: &Connection) -> RusqliteResult<Vec<RusqliteResult<FeedInfo>>> {
    let mut stmt = conn.prepare("SELECT * FROM FeedInfo")?;
    let data = stmt.query_map([], FeedInfo::new)?;
    Ok(data.collect())
}

async fn check_one_feed(
    bot: &Arc<Bot>,
    maybe_feed: RusqliteResult<FeedInfo>,
) -> Result<(), ToLogError> {
    let conn = get_db_connection()?;
    let feed_info = maybe_feed?;
    feed_info.update_scanned(&conn)?;
    let parser = FeedParser::new(feed_info.url.clone()).await?;
    let entries = db_helpers::FeedEntry::get_feed_entries(&conn, feed_info.id)?;
    let feed = parser.parse_feed(&max_age(), entries);
    update_feed_entries_dates(&conn, feed.new_dates, feed_info.id);
    send_new_entries_to_chat(conn, bot, &feed.title, feed.new_entries, feed_info).await?;
    Ok(())
}

fn update_feed_entries_dates(
    conn: &Connection,
    new_dates: HashMap<String, DateTime<Utc>>,
    feed_id: i32,
) {
    for (entry_id, new_date) in new_dates.into_iter() {
        let db_feed_entry = db_helpers::FeedEntry::new(feed_id, entry_id, new_date);
        if let Err(e) = db_feed_entry.update_time(conn, new_date) {
            error!("could not update entry time: {:?}", e);
        }
    }
}

async fn send_new_entries_to_chat(
    conn: Connection,
    bot: &Arc<Bot>,
    feed_title: &str,
    new_entries: Vec<FeedEntry>,
    db_feed_info: FeedInfo,
) -> Result<(), ToLogError> {
    let rooms = db_helpers::FeedRoom::get_feed_rooms(&conn, db_feed_info.id)?;
    let sender = EntrySender::new(bot, feed_title, rooms);
    for entry in new_entries.into_iter() {
        let add_result = db_helpers::FeedEntry::new(db_feed_info.id, entry.id.clone(), entry.time)
            .add_to_db(&conn);
        if let Err(e) = add_result {
            error!("could not save entry to db: {:?}", e);
        } else {
            if db_feed_info.is_scanned {
                sender.send(entry).await;
            }
        }
    }
    Ok(())
}

fn delete_old_entries(conn: &Connection) {
    let db_result = conn.execute("DELETE FROM FeedEntry WHERE time < ?1", [max_age()]);
    if let Err(e) = db_result {
        error!("error deleting old entries: {:?}", e);
    }
}

fn max_age() -> DateTime<Utc> {
    Utc::now()
        - ChronoDuration::days(
            env::var("MATRIX_RSS_ENTRY_MAX_DAYS")
                .unwrap_or("7".to_string())
                .parse()
                .unwrap_or(7),
        )
}
