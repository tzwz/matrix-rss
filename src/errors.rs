use thiserror::Error;

use feed_rs::parser::ParseFeedError;
use reqwest::Error as ReqwestError;
use rusqlite::Error as RusqliteError;

use matrix_sdk::{ruma::IdParseError, Error as MatrixSdkError};

#[derive(Error, Debug)]
pub enum ToLogError {
    #[error("failed to change db: {0}")]
    DbChangeError(#[from] RusqliteError),

    #[error("getting rss feed failed: {0}")]
    GettingFeedError(#[from] ReqwestError),

    #[error("parsing rss feed failed: {0}")]
    ParsingFeedError(#[from] ParseFeedError),

    #[error("could not get room id: {0}")]
    RoomIdError(#[from] IdParseError),

    #[error("could not perform matrix sdk action: {0}")]
    MatrixSdkError(#[from] MatrixSdkError),

    #[error("{0}")]
    InternalError(ToMatrixError),
}

#[derive(Error, Debug)]
pub enum ToMatrixError {
    #[error("Db operation failed: {0:?}")]
    DbError(#[from] RusqliteError),

    #[error("This feed is not in db")]
    NotInDbError,

    #[error("Feed action is missing for some reason, somehow")]
    FeedActionMissingError,

    #[error("Missing feed's url")]
    MissingUrl,

    #[error("Missing action (add/remove)")]
    MissingAction,
}

impl From<ToMatrixError> for ToLogError {
    fn from(err: ToMatrixError) -> Self {
        if let ToMatrixError::DbError(e) = err {
            Self::DbChangeError(e)
        } else {
            Self::InternalError(err)
        }
    }
}
