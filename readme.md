# matrix rss

Rss posts sending bot for matrix written in rust

## setting up
clone, `cp .env.defaults .env`, modify it to your liking, compile and run.

## usage

`.feed add url [show_content] [title]` -> adds feed to send updates from it in given room

**add** -> or `insert`, or `update`

**show_content** -> either `show` (or `content`, or `+content`, whatever you prefer) or `hide` (or `nocontent`, or `no_content`, or `-content`). This is optional, hidden is set by default

**title** -> basically anything that isn't url, some version of `show` or `hide` or `add`, it will be shown as from where given news is. This is optional, if not given then it will be taken from feed title tag, and if it's missing too, then it will be rss feed's host (and if there is problem with feed's host for some reason, then it will be feed's url).

This also works as a way to update given feed info (that's why `add` can be replaced by `update`), if there will be new title, it will be changed to new one, if there will be `show_content` value, it will be changed. If neither then title will be set to null.

`.feed delete url` -> removes feed from given room

**delete** -> or `del`, or `remove`, or `rm`

`.list` -> lists all feeds in given room (with their titles if they're set)

Instead of `.list` you can also use `.rsslist`, or `.feedslist`, or `.feedlist`, or `.listfeed`, or `.listfeeds`.

It should join automatically to rooms where it got invited and automatically remove room feed from db when kicked/banned.
